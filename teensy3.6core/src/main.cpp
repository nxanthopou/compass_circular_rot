/* Author: Xanthopoulos Nikitas, 2019.
 * License: GNU GPL v3.
 * Wiring and information about this file 
 * are described in repository's README.md.
 */

#include <Arduino.h>
#include <avr_emulation.h>
#include <math.h>
#include "i2c_t3.h"
#include "SparkFunLSM9DS1.h" // modified, removed SPI

#define LOOP_PERIOD 100000// usec
#define MAG_PERIOD 12500 // 80 Hz

typedef enum  {
    INIT,
    IDLE,
    GYRO_ACCEL,
    COMPASS
} t_loop_state;

enum {
    EXIT_NORMAL = 0,
    EXIT_ERROR = 1
};


int main(void);
void program_looper(void);
void compass_state(void);
void gyro_accel_state(void);
void idle_state(void);
void init_state(void);

extern LSM9DS1 initLSM(LSM9DS1 imu);
extern LSM9DS1 confLSMInterrupts(LSM9DS1 imu);
extern void printLSM(LSM9DS1 imu);

LSM9DS1 imu;
float heading, roll, pitch;
t_loop_state loop_state = INIT;
int loop_running = EXIT_NORMAL;

int main(void) {
    Serial.begin(115200); //debug serial
    delay(1000);
    imu = initLSM(imu);
    //default offsets read "by experiment"
    imu.setDefMagOffset();
    imu = confLSMInterrupts(imu);
    loop_state = IDLE;
    while (loop_running == EXIT_NORMAL)
        program_looper();
    return 0;
}

void program_looper() {
    unsigned long loop_start = micros();
    switch(loop_state) {
        case IDLE:
            idle_state();
            break;
        case GYRO_ACCEL:
            gyro_accel_state();
            break;
        case COMPASS:
            compass_state();
            break;
        case INIT:
            init_state();
            break;
        default:
            loop_running = EXIT_ERROR;
            break;
    }
    unsigned int micros_passed = micros() - loop_start;
    Serial.print("roll: ");
    Serial.print(roll);
    Serial.print(" pitch: ");
    Serial.print(pitch);
    Serial.print(" heading: ");
    Serial.print(heading);
    Serial.print(" usec: ");
    Serial.println(micros_passed);
}

void gyro_accel_state() {
    imu.readGyro();
    imu.readAccel();

    roll = atan2(imu.ay, imu.az);
    pitch = atan2(-imu.ax, sqrt(imu.ay * imu.ay + imu.az * imu.az));

    pitch *= 180.0 / PI;
    roll  *= 180.0 / PI;
    loop_state = IDLE;
}

void compass_state() {
    imu.readMag();
    heading = (imu.my == 0)? ((imu.mx < 0) ? PI : 0 ) : atan2(imu.mx, imu.my);

    if (heading > PI) heading -= (2 * PI);
    else if (heading < -PI) heading += (2 * PI);

    // Convert everything from radians to degrees:
    heading *= 180.0 / PI;
    loop_state = IDLE;
}


void idle_state() {
    //loop_state = GYRO_ACCEL;
    if (digitalRead(INT2_PIN_DRDY) == LOW) gyro_accel_state();
    if (digitalRead(RDYM_PIN) == HIGH) compass_state();
}


void init_state() {
    
    loop_state = IDLE;
}
