# Welcome
In this repository you can find software for building an open-source/hardware digital compass and circular movement sensor using a 9 Degrees of Freed inertial measurement unit. 

It is a part of a mobile robot I am working on, which will be open-{sourced/hardware} in the future.

## LSM9DS1 notes

`LSM9DS1` is a 9 Degrees of freedom inertial measurement unit consisting of 3-axis gyroscope, accelerometer, magnetometer. `I2C` and `SPI` are available.

[Sparkfun's Arduino library](https://github.com/sparkfun/SparkFun_LSM9DS1_Arduino_Library) has been modified to work with `Teensy 3.6`.

### Configuration

Gyroscope's/accelerometer's/magnetometers scales, sample rates, low/high pass filters, bandwidth, low/high power modes can be configured through `teensy3.6core/libraries/lsm9ds1/SparkFunLSM9DS1.cpp`, `LSM9DS1 initLSM(LSM9DS1 imu)`.

IMU's capabilities can be fully explored in root directory's manual `lsm9ds1.pdf`.

## Implementation notes

`Teensy` communicates with `LSM9DS1` through `I2C`. Gyroscope's/accelerometer's/magnetometer's output data registers (containing raw measurements) are read when corresponding `new data read` interrupst are triggered.

Magnetometer has been calibrated for my current space/location. Magnetometer calibration is key for its correct functionality, so measure maximum and minimum magnetic forces around the 3 axis and store them in `libraries/SparkFunLSM9DS1.cpp`, function: `void LSM9DS1::setDefMagOffset()`. 

Sample rate has been set to maximum for all of LSM's components, `952Hz` for gyroscope and accelerometer, `10Hz` for magnetometer.

The software architecture approaches a finite state conceptual. The central state is `IDLE`, changing to `GYRO_ACCEL` or `COMPASS` when data is read for any of them (if both, magnetometer gets served first since it has lower sample rate).

Acquiring data through `I2C` with `400KHz` bus clock takes ~600usec.

**Teensy 3.6** has been tested. 


## Wiring

LSM9DS1 Pin -> Teensy Pin:    
- `RDY` -> `27`, new magnetometer data ready  
- `INTM` -> `26`, magnetometer data exceeded a defined threshold  
- `INT1` -> `25`, new accel/gyro data read  
- `INT2` -> `24`, accel/gyro data exceeded a defined threshold  

## Deploying/running

### Bare metal notes
Software in this repository can be build using gcc-arm-none-eabi derivatives.

[As per cloned repository notes](https://github.com/djmdjm/teensy3):

> This is a minimal, bare-metal board support package for the Teensy 3.x
> ARM Cortex-M4 MK20DX{128|256) boards from https://www.pjrc.com/ 

> This is based on on work by Karl Lunt and Kevin Cuzner, but stripped
> back a bit further.

> "make load" will use an installed teensy_loader_cli to download the
> firmware to an attached Teensy.

> "make dump" will dump your program as assembler with interspersed
> code, which is handy when debugging or cycle-counting.

> Tested on Linux with https://launchpad.net/gcc-arm-embedded and on
> OpenBSD with ports/devel/arm-none-eabi/* (Linaro compiler)

### Steps to build and run

Ensure you have all of necessary gcc-none-eabi utilities, they can be found in `Makefile`.

Execute `make load` in `teensy3.6core/` directory, press the button on Teensy.

Execute `screen /dev/ttyACM0` to start receiving serial output from teensy, through USB. `screen` is a `GNU` utility that offers access to serial interfaces. 

`sparkfun` letters must face downwards, `LSM9DS1 Breakout Board` downwards for zero'ed roll measurements.

### Example output
![img](example_out.png)


# Additional libraries

[Enhanced I2C](https://github.com/nox771/i2c_t3) to read data through I2C.

# Licensing
Developed software is made available under [GNU GPL v3.0](http://www.gnu.org/licenses/gpl-3.0.html)
